from rest_framework import routers
from .api import CategoryViewset

router = routers.DefaultRouter()
router.register('api/category', CategoryViewset, 'category')

urlpatterns = router.urls