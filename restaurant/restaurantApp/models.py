from django.db import models

class Category(models.Model):
    category_name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)


class Menu_Item(models.Model):
    item_name = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='menu_items')
    description = models.CharField(max_length=500, blank=True)
    ingredients = models.CharField(max_length=500, blank=True)
    price = models.DecimalField(decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)


